export const FIELDS_MAP_ID = 'fieldsViewMap';
export const FIELDS_LIST_DEFAULT_NUMBER_TO_RENDER = 20;
export const FIELDS_LIST_ADDITIONAL_AMOUNT_TO_SHOW_ON_VIEW_MORE = 10;
export const FEET_PER_METER = 3.280839895;
export const SQUARE_METERS_PER_ACRE = 4046.856;
export const SQUARE_METERS_PER_HECTARE = 10000;
export const ORPHANED_DRAFT_SAMPLING_COLLECTION_INFO = `You no longer have access to the stratification this sampling collection was taken from. It may have been deleted, or the field it's related to may have been moved to a group you do not have access to. You can delete this sampling collection, or request that your project manager move the field back to a group you have access to so that you may resume the sampling collection.`;
