import { point } from '@turf/turf';
import { createMockArea, createMockMultiPolygon } from '../../../../tests/mockGenerators/index';
import { renderWithVuetify } from '../../../../tests/renderWithVuetify';
import AreaDrawEditor from './AreaDrawEditor.vue';
import { unfinalizeArea } from './drawUtils';

describe('AreaDrawEditor', () => {
	it('renders', () => {
		const { getByText } = renderWithVuetify(AreaDrawEditor, {
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		getByText('Next');
		getByText('Boundary');
		getByText('Cutout');
		getByText('Delete');
	});
	it('next button is disabled when drawFeatures is empty', () => {
		const { getByText } = renderWithVuetify(AreaDrawEditor, {
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		expect(getByText('Next').closest('button')).toBeDisabled();
	});
	it('clicking next button with valid drawFeatures emits "input" event with drawFeatures and "submit" event with finalized area', async () => {
		const area = createMockArea(createMockArea({ geometry: createMockMultiPolygon() }));
		const drawFeatures = unfinalizeArea(area);
		const { getByText, emitted } = renderWithVuetify(AreaDrawEditor, {
			propsData: {
				value: drawFeatures,
			},
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		await getByText('Next').click();
		expect(emitted().input[0][0]).toEqual(drawFeatures);
		const { id: _, ...actualAreaWithoutId } = emitted().submit[0][0];
		const { id: __, ...expectedAreaWithoutId } = area;
		expect(actualAreaWithoutId).toEqual(expectedAreaWithoutId);
	});

	it('it shows disabled next button for invalid drawFeatures', () => {
		const { getByText } = renderWithVuetify(AreaDrawEditor, {
			propsData: {
				value: point([-80, 40]),
			},
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		expect(getByText('Next').closest('button')).toBeDisabled();
	});
});
