import { booleanContains, combine, featureCollection, polygon } from '@turf/turf';
import { Feature, MultiPolygon, Polygon, Position } from 'geojson';

export enum DrawMode {
	Boundary = 'BOUNDARY',
	Cutout = 'CUTOUT',
	None = '',
}

function notEmpty<T>(value: T | null | undefined): value is T {
	return value !== null && value !== undefined;
}

export interface DrawError {
	id?: string | number;
	reason: string;
}

const cutoutIsContainedByBoundary = (cutout: Feature) => (boundary: Feature) => booleanContains(boundary, cutout);
const boundaryContainsCutout = (boundary: Feature) => (cutout: Feature) => booleanContains(boundary, cutout);
const isCutout = (feature: Feature): boolean => feature.properties?.type === DrawMode.Cutout;
const isBoundary = (feature: Feature): boolean => feature.properties?.type === DrawMode.Boundary;

export function validateDrawFeatures(drawFeatures: Feature[]): DrawError[] {
	const cutouts = drawFeatures.filter(isCutout);
	const boundaries = drawFeatures.filter(isBoundary);

	const cutoutErrors = cutouts
		.map((cutout: Feature) => {
			const isContainedByBoundary = cutoutIsContainedByBoundary(cutout);
			// TODO: first merge boundaries with turf.combine?
			return boundaries.some(isContainedByBoundary)
				? null
				: { id: cutout.id, reason: 'Cutout is not contained by one boundary.' };
		})
		.filter(notEmpty);

	return [...cutoutErrors];
}

export function finalizeArea(drawFeatures: Feature<Polygon>[]) {
	const cutouts = drawFeatures.filter(isCutout);
	const boundaries = drawFeatures.filter(isBoundary);

	const boundariesWithCutouts = boundaries.map((boundary) => {
		const containsCutout = boundaryContainsCutout(boundary);
		const associatedHoles = cutouts
			.filter(containsCutout)
			// map cutout Feature => Position[] (exterior closed LineString)
			.map((cutout: Feature<Polygon>): Position[] => cutout.geometry.coordinates[0]);

		return {
			type: boundary.type,
			properties: {},
			geometry: {
				type: boundary.geometry.type,
				coordinates: [boundary.geometry.coordinates[0], ...associatedHoles],
			},
		};
	});

	const combined = combine(featureCollection(boundariesWithCutouts));
	// strip properties, `turf.combine` collects properties from combined features
	return combined.features.map((feature) => ({ ...feature, properties: {} }))[0];
}

export function unfinalizeArea(area: Feature<MultiPolygon>): Feature<Polygon>[] {
	const { boundaryRings, cutoutRings } = area.geometry.coordinates.reduce<{
		boundaryRings: Position[][];
		cutoutRings: Position[][];
	}>(
		(flattened, coords: Position[][]) => {
			const [boundary, ...holes] = coords;
			return {
				boundaryRings: [...flattened.boundaryRings, boundary],
				cutoutRings: [...flattened.cutoutRings, ...holes],
			};
		},
		{ boundaryRings: [], cutoutRings: [] }
	);

	return [
		...boundaryRings.map((boundaryRing, index) =>
			polygon([boundaryRing], { type: DrawMode.Boundary }, { id: `Boundary-${index}` })
		),
		...cutoutRings.map((cutoutRing, index) =>
			polygon([cutoutRing], { type: DrawMode.Cutout }, { id: `Cutout-${index}` })
		),
	];
}

export function createPopupContent(
	id: string | number | undefined,
	reason: string,
	deleteFeatureHandler: (id: string) => void,
	popupRemoveHandler: () => void
) {
	const popupContent = document.createElement('div');

	const reasonIcon = document.createElement('i');
	reasonIcon.className = 'v-icon notranslate mdi mdi-alert theme--light error--text pr-1 mt-n1';
	reasonIcon.style.fontSize = '16px';

	const reasonText = document.createElement('span');
	reasonText.className = 'reason';
	reasonText.innerHTML = reason;

	const deleteButton = document.createElement('button');
	deleteButton.innerText = 'Delete';
	deleteButton.className = 'text-decoration-underline d-inline-block ml-1';
	deleteButton.onclick = () => {
		deleteFeatureHandler(String(id));
		popupRemoveHandler();
	};

	popupContent.append(reasonIcon, reasonText, deleteButton);
	return popupContent;
}
