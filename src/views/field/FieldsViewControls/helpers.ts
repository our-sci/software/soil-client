import { ContactPoint, FieldRow } from '@/store/types';
import { dataToCSV, download, generateFilename } from '../../../utils/exportFile';

function flattenContactPoint(contactPoint: ContactPoint, index: number) {
	return {
		[`Contact Phone ${index + 1}`]: contactPoint.telephone,
		[`Contact Email ${index + 1}`]: contactPoint.email,
	};
}

function flattenField(field: FieldRow) {
	return {
		'Field Name': field.name,
		'Producer Name': field.producerName,
		Group: field.groupName,
		Status: field.status.state,
		'Field Centroid Latitude': field.lngLat?.[1] ?? '',
		'Field Centroid Longitude': field.lngLat?.[0] ?? '',
		...field.contactPoints.map(flattenContactPoint).reduce((prev, cur) => ({ ...prev, ...cur }), {}),
	};
}

export function generateFieldsCSV(fields: FieldRow[]) {
	const rows = fields.map(flattenField);
	return dataToCSV(rows);
}

export function downloadFieldsCSV(fields: Array<FieldRow>) {
	const filename = generateFilename('fields');
	download(filename, generateFieldsCSV(fields), 'csv');
}
