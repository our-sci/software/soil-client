import { createMockContactPoint, createMockFieldRow } from '../../../../tests/mockGenerators';
import { generateFieldsCSV } from './helpers';

describe('downloadFieldsCSV', () => {
	const mockFieldRows = [
		createMockFieldRow({
			name: 'field1',
			producerName: 'producer1',
			groupName: 'group1',
			lngLat: [1, 2],
			contactPoints: [
				createMockContactPoint({ telephone: '123-4567', email: 'a@b.c' }),
				createMockContactPoint({ telephone: '456-4567', email: 'c@d.e' }),
			],
		}),
		createMockFieldRow({
			name: 'field2',
			producerName: 'producer2',
			groupName: 'group2',
			lngLat: [3, 4],
			contactPoints: [createMockContactPoint({ telephone: '789-2345', email: 'b@c.d' })],
		}),
	];

	it('matches snapshot', () => {
		expect(generateFieldsCSV(mockFieldRows)).toMatchSnapshot();
	});

	it('enumerates multiple contact points with numbered headers for each', () => {
		const csv = generateFieldsCSV(mockFieldRows);

		expect(csv).toContain('Contact Phone 1');
		expect(csv).toContain('Contact Email 1');
		expect(csv).toContain('Contact Phone 2');
		expect(csv).toContain('Contact Email 2');
	});
});
