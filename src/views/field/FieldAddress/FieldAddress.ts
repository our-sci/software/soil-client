import { Address, Area } from '@/store/types';
import { prettyGoodCenterOf } from '@/utils/geo';

const createGoogleMapsUrl = (query: string): string => `https://www.google.com/maps/search/?api=1&query=${query}`;

const createContactAddressHref = (address: Address): string => {
	const { streetAddress, addressLocality, addressRegion, postalCode } = address;
	if (!streetAddress || !addressLocality) {
		return '';
	}

	const query = `${streetAddress},+${addressLocality}${addressRegion ? `,+${addressRegion}` : ''}${
		postalCode ? `+${postalCode}` : ''
	}`;

	return createGoogleMapsUrl(query);
};

const createFieldLocationHref = (area: Area): string => {
	const fieldCenter = prettyGoodCenterOf(area);
	const [lng, lat] = fieldCenter?.geometry?.coordinates || [];
	const query = encodeURIComponent(`${lat},${lng}`);
	return createGoogleMapsUrl(query);
};

export { createContactAddressHref, createFieldLocationHref };
