<template>
	<div class="StratificationDetails">
		<collect-map
			:area="area"
			:locations="{
				type: 'FeatureCollection',
				features: locations,
			}"
		/>
		<v-card-title class="d-block d-sm-flex align-center justify-space-between">
			<div>{{ field.name }}</div>
			<download-stratification-artifacts v-if="artifactKeys" :artifactKeys="artifactKeys.value" />
		</v-card-title>
		<v-card-text class="flex-grow-1">
			No. of Locations: {{ locations.length }}<br />
			No. of Strata: {{ nStrata }}<br />
			Area of field: {{ fieldAreaByUnit.hectares }} ha ({{ fieldAreaByUnit.acres }} acres)
			<template v-if="bufferedFieldAreaByUnit">
				<br />
				Buffered Area of Field: {{ bufferedFieldAreaByUnit.hectares }} ha ({{ bufferedFieldAreaByUnit.acres }} acres)
			</template>
			<template v-if="areaByStratum">
				<div v-for="([stratum, areaByUnit], i) in Object.entries(areaByStratum)" :key="i">
					Area of Stratum {{ stratum }}: {{ areaByUnit.hectares }} ha ({{ areaByUnit.acres }} acres)
				</div>
			</template>
		</v-card-text>
		<v-expansion-panels class="px-2 mb-4">
			<v-expansion-panel v-if="formattedStratification">
				<v-expansion-panel-header>Stratification Data</v-expansion-panel-header>
				<v-expansion-panel-content>
					<pre>{{ formattedStratification }}</pre>
				</v-expansion-panel-content>
			</v-expansion-panel>
			<v-expansion-panel v-if="formattedStratificationConfig">
				<v-expansion-panel-header>Stratification Configuration</v-expansion-panel-header>
				<v-expansion-panel-content>
					<pre>{{ formattedStratificationConfig }}</pre>
				</v-expansion-panel-content>
			</v-expansion-panel>
		</v-expansion-panels>
		<a class="d-block text-sm-right mt-8 mb-2 mx-3 text-body-1" @click="downloadLocationsCSV">Download Locations CSV</a>
		<v-data-table :headers="tableHeaders" :items="tableData"></v-data-table>
	</div>
</template>

<script lang="ts">
import { defineComponent } from '@vue/composition-api';
import { area as turfArea } from '@turf/turf';
import CollectMap from '@/components/map/CollectMap.vue';
import { SQUARE_METERS_PER_ACRE, SQUARE_METERS_PER_HECTARE } from '../../constants';
import { Area, Field, Location, Stratification, StratificationInput } from '../../store/types';
import { dataToCSV, download } from '../../utils/exportFile';
import DownloadStratificationArtifacts from './DownloadStratificationArtifacts.vue';

const getFieldAreaByUnit = (stratification: Stratification, area: Area) => {
	const rawHectares = stratification.result.find(({ name }) => name === 'FIELD_BOUNDARY_AREA_HECTARES')
		?.value as number;
	// demo stratifications and older stratifications may not have FIELD_BOUNDARY_AREA_HECTARES
	if (!rawHectares) {
		const squareMeters = area ? turfArea(area) : 0;
		const acres = (squareMeters / SQUARE_METERS_PER_ACRE).toFixed(2);
		const hectares = (squareMeters / SQUARE_METERS_PER_HECTARE).toFixed(2);
		return { acres, hectares };
	} else {
		const hectares = rawHectares.toFixed(2);
		const acres = ((rawHectares * SQUARE_METERS_PER_HECTARE) / SQUARE_METERS_PER_ACRE).toFixed(2);
		return { acres, hectares };
	}
};

const getBufferedFieldAreaByUnit = (stratification: Stratification) => {
	const rawHectares = stratification.result.find(({ name }) => name === 'FIELD_BOUNDARY_AREA_BUFFERED_HECTARES')
		?.value as number;
	// demo stratifications and older stratifications may not have FIELD_BOUNDARY_AREA_BUFFERED_HECTARES
	if (!rawHectares) {
		return null;
	}
	return {
		hectares: rawHectares.toFixed(2),
		acres: ((rawHectares * SQUARE_METERS_PER_HECTARE) / SQUARE_METERS_PER_ACRE).toFixed(2),
	};
};

const getAreaByStratum = (stratification: Stratification) => {
	const hectaresByStratum = stratification.result.find(({ name }) => name === 'STRATA_AREAS_HECTARES')?.value as number;
	// demo stratifications and older stratifications may not have STRATA_AREAS_HECTARES
	if (!hectaresByStratum) {
		return null;
	}
	return Object.fromEntries(
		Object.entries(hectaresByStratum).map(([stratum, ha]) => [
			stratum,
			{
				hectares: ha.toFixed(2),
				acres: ((ha * SQUARE_METERS_PER_HECTARE) / SQUARE_METERS_PER_ACRE).toFixed(2),
			},
		])
	);
};

export default defineComponent({
	components: {
		CollectMap,
		DownloadStratificationArtifacts,
	},
	props: {
		locations: {
			required: false,
			type: Array as () => Location[],
		},
		stratificationConfig: {
			required: false,
			type: String,
		},
		field: {
			required: true,
			type: Object as () => Field,
		},
		area: {
			required: true,
			type: Object as () => Area,
		},
		stratification: {
			required: true,
			type: Object as () => Stratification,
		},
	},
	setup(props: any) {
		const formattedStratificationConfig = props.stratificationConfig
			? JSON.stringify(JSON.parse(props.stratificationConfig), null, 2)
			: null;
		const formattedStratification = JSON.stringify(props.stratification, null, 2);
		const nStrata =
			props.stratification.input.find(({ name }: StratificationInput) => name === 'CLUSTERING_NUMBER_OF_CLUSTERS')
				?.value ??
			props.locations.reduce((r: Set<string>, x: any) => {
				r.add(x.properties.stratum);
				return r;
			}, new Set()).size;

		const fieldAreaByUnit = getFieldAreaByUnit(props.stratification, props.area);
		const bufferedFieldAreaByUnit = getBufferedFieldAreaByUnit(props.stratification);
		const areaByStratum = getAreaByStratum(props.stratification);

		const tableData = props.locations.map((location: any, i: number) => ({
			lat: location.geometry.coordinates[1],
			lng: location.geometry.coordinates[0],
			stratum: location.properties.stratum,
			label: location.properties.label ?? `${i + 1}`,
		}));
		const tableHeaders = [
			{ text: 'Latitude', value: 'lat' },
			{ text: 'Longitude', value: 'lng' },
			{ text: 'Stratum', value: 'stratum' },
			{ text: 'Label', value: 'label' },
		];

		const downloadLocationsCSV = () => {
			const filename = `${props.field.name} locations.csv`;
			download(filename, dataToCSV(tableData), 'csv');
		};

		const artifactKeys = props.stratification.result.find(({ name }: StratificationInput) => name === 'ARTIFACT_KEYS');

		return {
			formattedStratification,
			formattedStratificationConfig,
			nStrata,
			fieldAreaByUnit,
			bufferedFieldAreaByUnit,
			areaByStratum,
			tableData,
			tableHeaders,
			downloadLocationsCSV,
			artifactKeys,
		};
	},
});
</script>

<style>
.StratificationDetails pre {
	white-space: pre-wrap;
}
</style>
