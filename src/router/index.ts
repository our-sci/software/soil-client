import { Store } from 'vuex';
import Vue from 'vue';
import VueRouter, { NavigationGuard, RouteConfig } from 'vue-router';
import multiguard from 'vue-router-multiguard';
import AppHeader from '@/components/common/AppHeader.vue';
import { RootState } from '@/store/types';
import Admin from '@/views/admin/Admin.vue';
import ForgotPassword from '@/views/auth/ForgotPassword.vue';
import Login from '@/views/auth/Login.vue';
import Profile from '@/views/auth/Profile.vue';
import Register from '@/views/auth/Register.vue';
import ResetPassword from '@/views/auth/ResetPassword.vue';
import UpdatePassword from '@/views/auth/UpdatePassword.vue';
import Debug from '@/views/debug/Debug.vue';
import FieldCreate from '@/views/field/FieldCreate.vue';
import FieldShow from '@/views/field/FieldShow.vue';
import FieldsView from '@/views/field/FieldsView.vue';
import GroupCreate from '@/views/group/GroupCreate.vue';
import GroupList from '@/views/group/GroupList.vue';
import GroupShow from '@/views/group/GroupShow.vue';
import MembershipCreate from '@/views/group/MembershipCreate.vue';
import SamplingCollectionCreate from '@/views/sampling-collection/SamplingCollectionCreate.vue';
import SamplingCollectionEdit from '@/views/sampling-collection/SamplingCollectionEdit.vue';
import SamplingCollectionList from '@/views/sampling-collection/SamplingCollectionList.vue';
import SamplingCollectionShow from '@/views/sampling-collection/SamplingCollectionShow.vue';
import Settings from '@/views/settings/Settings.vue';
import SuperAdmin from '@/views/super-admin/SuperAdmin.vue';
import { AuthService } from '../services/storageService';

Vue.use(VueRouter);

const loginRequiredGuard: NavigationGuard = async (to, from, next) => {
	if (!AuthService.getToken()) {
		next({ name: 'auth-login', params: { redirect: to.path } });
	} else {
		next();
	}
};

const createGetResourcesGuard = (store: Store<RootState>): NavigationGuard => async (to, from, next) => {
	// TODO: We should probably only getAllResources if resources/isLoaded is false,
	// but first we need to make sure there aren't any routes relying on the current behavior
	// One such route might be SamplingCollectionShow, which might rely on fetching the new sampling collection
	// when the user is redirected to SamplingCollectionShow after submitting a Sampling Collection.
	store.dispatch('resources/getAllResources');
	next();
};

const createRoutes = ({ getResourcesGuard }: { getResourcesGuard: NavigationGuard }): Array<RouteConfig> => [
	{ path: '/', redirect: '/fields' },
	{
		path: '/fields',
		name: 'Fields',
		components: {
			default: FieldsView,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Fields',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/field/new',
		name: 'Add Field',
		components: {
			default: FieldCreate,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Add a field',
			},
		},
	},
	{
		path: '/field/:id',
		name: 'Field Show',
		components: {
			default: FieldShow,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Field',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/create-sampling-collection',
		name: 'Sampling Collection Create',
		components: {
			default: SamplingCollectionCreate,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Collect New',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/sampling-collection/:samplingCollectionId/create',
		name: 'New Sampling Collection',
		components: {
			default: SamplingCollectionEdit,
			appHeader: AppHeader,
		},
		props: {
			default: (route) => ({
				creating: true,
				samplingCollectionId: route.params.samplingCollectionId,
			}),
			appHeader: {
				title: 'Collect',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/sampling-collection/:samplingCollectionId/edit',
		name: 'Sampling Collection Edit',
		components: {
			default: SamplingCollectionEdit,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Collect',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/sampling-collection/:id',
		name: 'Sampling Collection Show',
		components: {
			default: SamplingCollectionShow,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Sampling Collection',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/sampling-collection/',
		name: 'Sampling Collection List',
		components: {
			default: SamplingCollectionList,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Sampling Collections',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/debug',
		name: 'Debug',
		components: {
			default: Debug,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Debug Options',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/groups',
		name: 'Groups',
		components: {
			default: GroupList,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Groups',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/groups/:parentGroupId?/new',
		name: 'Add Group',
		components: {
			default: GroupCreate,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Create new group',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/groups/:id',
		name: 'Group Show',
		components: {
			default: GroupShow,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Groups',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/groups/:groupId/membership/new',
		name: 'Add Member',
		components: {
			default: MembershipCreate,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Add a member',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/settings',
		name: 'Settings',
		components: {
			default: Settings,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Settings',
			},
		},
	},
	{
		path: '/admin',
		name: 'Admin',
		components: {
			default: Admin,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Move Fields',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/super-admin',
		name: 'Super Admin',
		components: {
			default: SuperAdmin,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Super Admin',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/auth/login',
		name: 'auth-login',
		components: {
			default: Login,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Login',
			},
		},
	},
	{
		path: '/auth/register',
		name: 'auth-register',
		components: {
			default: Register,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Register',
			},
		},
	},
	{
		path: '/auth/profile',
		name: 'auth-profile',
		components: {
			default: Profile,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Profile',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/auth/forgot-password',
		name: 'auth-forgot-password',
		components: {
			default: ForgotPassword,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Reset Password',
			},
		},
	},
	{
		path: '/auth/reset-password',
		name: 'auth-reset-password',
		components: {
			default: ResetPassword,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Reset Password',
			},
		},
	},
	{
		path: '/auth/change-password',
		name: 'change-password',
		components: {
			default: UpdatePassword,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Change Password',
			},
		},
	},
];

export default function createRouter(store: Store<RootState>) {
	const getResourcesGuard = createGetResourcesGuard(store);
	return new VueRouter({
		mode: 'history',
		base: process.env.BASE_URL,
		routes: createRoutes({ getResourcesGuard }),
	});
}
