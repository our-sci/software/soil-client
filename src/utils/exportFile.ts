import stringify from 'csv-stringify/lib/sync';

export function generateFilename(prefix: string, extension = 'csv') {
	return `${prefix}-${new Date().toISOString().replaceAll(/:|\./g, '-')}.${extension}`;
}

export function generateDataURI(data: string, type: string) {
	const uriEncodedData = encodeURIComponent(data);
	switch (type) {
		case 'csv':
			return `data:text/csv;charset=utf-8,${uriEncodedData}`;
		case 'json':
			return `data:text/json;,${uriEncodedData}`;
		default:
			throw new TypeError(`generateDataURI: unknown type ${type}`);
	}
}

export function dataToCSV(data: Array<object>, headers?: string[]) {
	const columnHeadersPresentInData = [...new Set(data.flatMap(Object.keys))];
	const sortedColumnHeaders = headers?.filter((header) => columnHeadersPresentInData.includes(header));
	const columns = Array.isArray(headers) ? sortedColumnHeaders : columnHeadersPresentInData;
	return stringify(data, {
		columns,
		header: true,
	});
}

export function download(filename: string, data: string, type: string) {
	const anchor = document.createElement('a');
	anchor.target = '_blank';
	anchor.download = filename;
	anchor.href = generateDataURI(data, type);
	anchor.click();
}
