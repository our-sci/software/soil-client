import { dataToCSV, generateDataURI } from './exportFile';

describe('dataToCSV', () => {
	describe('when headers is not set', () => {
		it('contains all the keys that are present in any row as headers', () => {
			const data = [
				{
					a: 'a',
					b: 'b',
					c: 'c',
				},
				{
					a: 'a',
					b: 'b',
					c: 'c',
					d: 'd',
				},
			];

			const csv = dataToCSV(data);

			expect(csv).toContain('a,b,c,d');
		});
	});

	describe('when headers is set', () => {
		it('does not include headers from data that are not in passed in headers', () => {
			const data = [
				{
					a: 'a',
					b: 'b',
					c: 'c',
				},
				{
					a: 'a',
					b: 'b',
					c: 'c',
					d: 'd',
				},
			];

			const csv = dataToCSV(data, ['a', 'b']);

			expect(csv).not.toContain('c');
			expect(csv).not.toContain('d');
		});

		it('does not include headers from passed in headers that are not in the data', () => {
			const data = [
				{
					a: 'a',
					b: 'b',
				},
			];

			const csv = dataToCSV(data, ['a', 'b', 'c']);

			expect(csv).not.toContain('c');
		});

		it('includes headers if they are present in any row of data', () => {
			const data = [
				{
					a: 'a',
					b: 'b',
				},
				{
					a: 'a',
					c: 'c',
				},
			];

			const csv = dataToCSV(data, ['a', 'b', 'c']);

			expect(csv).toContain('b');
			expect(csv).toContain('c');
		});

		it('orders headers according to the order of the headers argument, not the order of keys in the passed in data', () => {
			const data = [
				{
					c: 'c',
					b: 'b',
					a: 'a',
				},
			];

			const result = dataToCSV(data, ['a', 'c', 'b']);

			expect(result).toContain('a,c,b');
		});
	});
});

describe('generateDataURI', () => {
	it('uri encodes data', () => {
		const inputWithIllegalURIcharacters = ` ?\n`;

		const encodedURI = generateDataURI(inputWithIllegalURIcharacters, 'csv');

		expect(encodedURI).toContain('%20');
		expect(encodedURI).toContain('%3F');
		expect(encodedURI).toContain('%0A');
	});

	it('throws an error if type is unknown', () => {
		expect(() => generateDataURI('', 'exe')).toThrow(TypeError);
	});
});
