function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
	return value !== null && value !== undefined;
}

const isRejected = (input: PromiseSettledResult<unknown>): input is PromiseRejectedResult =>
	input.status === 'rejected';
const isFulfilled = <T>(input: PromiseSettledResult<T>): input is PromiseFulfilledResult<T> =>
	input.status === 'fulfilled';

export { isFulfilled, isRejected, notEmpty };
