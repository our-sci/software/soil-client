const OFFLINE_MAPS_CACHE_NAME = 'offline-maps-runtime';

const cachedUrls: Promise<string[]> = (async function () {
	const cache = await window.caches.open(OFFLINE_MAPS_CACHE_NAME);
	const keys = await cache.keys();
	return keys.map((key) => key.url);
})();

export function getCachedUrls() {
	return cachedUrls;
}
