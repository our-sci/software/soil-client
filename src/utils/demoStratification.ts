import { randomPoint } from '@turf/random';
import { bbox, buffer, Point, pointsWithinPolygon } from '@turf/turf';
import { Feature, Polygon } from 'geojson';
import { Area, Field } from '@/store/types';

function generateRandomPointsInPolygon(polygon: Polygon, numberOfPoints = 10, bufferMeters = 5) {
	const bufferedPolygon = buffer(polygon, -bufferMeters / 1000);
	const bufferedBbox = bbox(bufferedPolygon);
	const pointsInPolygon: Feature<Point, any>[] = [];
	let remainder = numberOfPoints;
	while (pointsInPolygon.length < numberOfPoints) {
		const points = randomPoint(remainder, { bbox: bufferedBbox });
		const ptsInPoly = pointsWithinPolygon(points, bufferedPolygon);
		pointsInPolygon.push(...ptsInPoly.features.slice(0, remainder));
		remainder -= ptsInPoly.features.length;
	}
	return pointsInPolygon;
}

function generateDemoStratification(field: Field, area: Area, numberOfPoints = 3) {
	return {
		stratification: {
			name: `Field ${field.name} Demo Stratification (random points)`,
			agent: 'soil-client',
			dateCreated: new Date().toISOString(),
			provider: 'OUR_SCI',
			isDemo: true,
			algorithm: {
				name: 'generateRandomPointsInPolygon',
				alternateName: '',
				codeRepository: '',
				version: '0.0.1',
				doi: '',
			},
			input: [
				{
					name: 'numberOfPoints',
					value: numberOfPoints,
				},
			],
		},
		locationCollection: {
			features: generateRandomPointsInPolygon(area.geometry, numberOfPoints),
		},
	};
}

export { generateDemoStratification, generateRandomPointsInPolygon };
