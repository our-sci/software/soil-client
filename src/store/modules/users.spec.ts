import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createUsersModule, { actions, useUsers } from './users';

jest.mock('../../services/soilApiService');

describe('users module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('getUsers', () => {
			it('commits requestGetUsers, then requestGetUsersSuccess on getUsers success', async () => {
				(soilApiService.getUsers as jest.Mock).mockResolvedValueOnce({ data: [] });

				await (actions.getUsers as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetUsers');
				expect(commitSpy).toHaveBeenCalledWith('requestGetUsersSuccess', []);
			});

			it('commits requestGetUsers, then requestGetUsersError on getUsers error', async () => {
				(soilApiService.getUsers as jest.Mock).mockRejectedValueOnce(new Error('error'));

				await (actions.getUsers as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetUsers');
				expect(commitSpy).toHaveBeenCalledWith('requestGetUsersError', new Error('error'));
			});
		});
	});

	describe('mutations', () => {
		let usersModule: any;

		beforeEach(() => {
			usersModule = createUsersModule();
		});

		describe('requestGetUsers', () => {
			it('sets isLoading to true, isLoaded to false, and error to null', () => {
				usersModule.mutations.requestGetUsers(usersModule.state);

				expect(usersModule.state.isLoading).toBe(true);
				expect(usersModule.state.isLoaded).toBe(false);
				expect(usersModule.state.error).toBe(null);
			});
		});

		describe('requestGetUsersSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null, and users to the passed in users', () => {
				usersModule.mutations.requestGetUsersSuccess(usersModule.state, []);

				expect(usersModule.state.isLoading).toBe(false);
				expect(usersModule.state.isLoaded).toBe(true);
				expect(usersModule.state.error).toBe(null);
				expect(usersModule.state.users).toEqual([]);
			});
		});

		describe('requestGetUsersError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error, and users to null', () => {
				usersModule.mutations.requestGetUsersError(usersModule.state, new Error('error'));

				expect(usersModule.state.isLoading).toBe(false);
				expect(usersModule.state.isLoaded).toBe(false);
				expect(usersModule.state.error).toEqual(new Error('error'));
				expect(usersModule.state.users).toEqual([]);
			});
		});
	});

	describe('getters', () => {
		let usersModule: any;

		beforeEach(() => {
			usersModule = createUsersModule();
		});

		describe('getIsLoading', () => {
			it('returns the isLoading state', () => {
				usersModule.state.isLoading = true;

				expect(usersModule.getters.getIsLoading(usersModule.state)).toBe(true);
			});
		});

		describe('getIsLoaded', () => {
			it('returns the isLoaded state', () => {
				usersModule.state.isLoaded = true;

				expect(usersModule.getters.getIsLoaded(usersModule.state)).toBe(true);
			});
		});

		describe('getHasError', () => {
			it('returns true if the error state is not null', () => {
				usersModule.state.error = new Error('error');

				expect(usersModule.getters.getHasError(usersModule.state)).toBe(true);
			});

			it('returns false if the error state is null', () => {
				usersModule.state.error = null;

				expect(usersModule.getters.getHasError(usersModule.state)).toBe(false);
			});
		});

		describe('getError', () => {
			it('returns the error state', () => {
				usersModule.state.error = new Error('error');

				expect(usersModule.getters.getError(usersModule.state)).toEqual(new Error('error'));
			});
		});

		describe('getUsers', () => {
			it('returns the users state', () => {
				usersModule.state.users = [];

				expect(usersModule.getters.getUsers(usersModule.state)).toEqual([]);
			});
		});
	});

	describe('hooks', () => {
		describe('useUsers', () => {
			it('returns computedRefs of users state and dispatches users/getUsers action if users are not loading or loaded', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();

				const actual = useUsers(store);

				expect(actual.isLoading.value).toBe(false);
				expect(actual.isLoaded.value).toBe(false);
				expect(actual.hasError.value).toBe(false);
				expect(actual.users.value).toEqual([]);
				expect(actual.error.value).toBe(null);
				expect(store.dispatch).toHaveBeenCalledWith('users/getUsers');
			});

			it('does NOT dispatch users/getUsers action if users are loading', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				store.replaceState({
					...store.state,
					users: {
						...store.state.users,
						isLoading: true,
					},
				});

				useUsers(store);

				expect(store.dispatch).not.toHaveBeenCalled();
			});

			it('does NOT dispatch users/getUsers action if users are loaded', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				store.replaceState({
					...store.state,
					users: {
						...store.state.users,
						isLoaded: true,
					},
				});

				useUsers(store);

				expect(store.dispatch).not.toHaveBeenCalled();
			});
		});
	});
});
