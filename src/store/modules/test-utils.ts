import { ApiKey, AsyncState, UserWithApiKeys } from '../types';
import { ApiKeysState } from './apiKeys';
import { AreasState } from './areas';
import { FieldState } from './fields';
import { GroupsState } from './group';
import { ResourcesState } from './resources';
import { SamplesState } from './samples';

export const createAsyncState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
}: Partial<AsyncState> = {}): AsyncState => ({
	isLoading,
	isLoaded,
	error,
});

export const createMockResourcesState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
	submitIsLoading = false,
	submitError = null,
	fields = [],
	areas = [],
	stratifications = [],
	locationCollections = [],
	locations = [],
	samples = [],
	samplings = [],
	samplingCollections = [],
}: Partial<ResourcesState> = {}): ResourcesState => ({
	isLoading,
	isLoaded,
	error,
	submitIsLoading,
	submitError,
	fields,
	areas,
	stratifications,
	locationCollections,
	locations,
	samples,
	samplings,
	samplingCollections,
});

export const createMockGroupsState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
	groups = [],
	groupTree = [],
}: Partial<GroupsState> = {}): GroupsState => ({
	isLoading,
	isLoaded,
	error,
	groups,
	groupTree,
});

export const createMockFieldState = (state: Partial<FieldState> = {}): FieldState => {
	return {
		delete: state.delete ?? createAsyncState(),
		edit: state.edit ?? createAsyncState(),
		create: state.create ?? createAsyncState(),
	};
};

export const createMockAreasState = (state: Partial<AreasState> = {}): AreasState => {
	return {
		edit: state.edit ?? createAsyncState(),
	};
};

export const createMockSamplesState = (state: Partial<SamplesState> = {}): SamplesState => {
	const { isLoaded, isLoading, error } = createAsyncState();
	return {
		records: state.records ?? [],
		isLoading: state.isLoading ?? isLoading,
		isLoaded: state.isLoaded ?? isLoaded,
		error: state.error ?? error,
		edit: state.edit ?? { isLoaded, isLoading, error },
	};
};

export const createMockApiKey = (apiKey: Partial<ApiKey> = {}): ApiKey => ({
	id: 'mock api key id',
	label: 'mock api key label',
	...apiKey,
});

export const createMockUser = (user: Partial<UserWithApiKeys> = {}): UserWithApiKeys => ({
	id: 'mock user id',
	email: 'mock user email',
	seen: true,
	apiKeys: [createMockApiKey()],
	...user,
});

export const createMockApiKeysState = (state: Partial<ApiKeysState> = {}): ApiKeysState => {
	return {
		create: state.create ?? createAsyncState(),
		revoke: state.revoke ?? createAsyncState(),
	};
};
