import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import soilApiService from '../../services/soilApiService';
import { AsyncState, RootState, UserWithApiKeys } from '../types';

export interface UserState extends AsyncState {
	user: UserWithApiKeys | null;
}

function createInitialState(): UserState {
	return {
		user: null,
		isLoading: false,
		isLoaded: false,
		error: null,
	};
}

const actions: ActionTree<UserState, RootState> = {
	async getUser({ commit }): Promise<void> {
		commit('requestGetUser');
		try {
			const { data: user } = await soilApiService.getUser();
			commit('requestGetUserSuccess', user);
		} catch (error) {
			commit('requestGetUserError', error);
		}
	},
};

const mutations: MutationTree<UserState> = {
	requestGetUser(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestGetUserSuccess(state, user: UserWithApiKeys) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
		state.user = Object.freeze(user);
	},
	requestGetUserError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
		state.user = null;
	},
};

const getters: GetterTree<UserState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
	getError: (state) => state.error,
	getUser: (state) => state.user,
};

const createUserModule = (): Module<UserState, RootState> => ({
	namespaced: true,
	state: createInitialState(),
	actions,
	mutations,
	getters,
});

const useUser = (store: Store<RootState>) => {
	const user = computed(() => store.getters['user/getUser']);
	const isLoading = computed(() => store.getters['user/getIsLoading']);
	const isLoaded = computed(() => store.getters['user/getIsLoaded']);
	const hasError = computed(() => store.getters['user/getHasError']);

	if (!isLoaded.value && !isLoading.value && !hasError.value) {
		store.dispatch('user/getUser');
	}

	return {
		user,
		isLoading,
		isLoaded,
		hasError,
	};
};

export { actions, getters, mutations, useUser };

export default createUserModule;
