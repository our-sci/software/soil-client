import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import soilApiService from '../../services/soilApiService';
import { AsyncState, RootState, UserWithApiKeys } from '../types';

export interface UsersState extends AsyncState {
	users: readonly UserWithApiKeys[];
}

function createInitialState(): UsersState {
	return {
		users: [],
		isLoading: false,
		isLoaded: false,
		error: null,
	};
}

const actions: ActionTree<UsersState, RootState> = {
	async getUsers({ commit }): Promise<void> {
		commit('requestGetUsers');
		try {
			const { data: users } = await soilApiService.getUsers();
			commit('requestGetUsersSuccess', users);
		} catch (error) {
			commit('requestGetUsersError', error);
		}
	},
};

const mutations: MutationTree<UsersState> = {
	requestGetUsers(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestGetUsersSuccess(state, users: UserWithApiKeys[]) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
		state.users = Object.freeze(users);
	},
	requestGetUsersError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
		state.users = [];
	},
};

const getters: GetterTree<UsersState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
	getError: (state) => state.error,
	getUsers: (state) => state.users,
};

const createUsersModule = (): Module<UsersState, RootState> => ({
	namespaced: true,
	state: createInitialState(),
	actions,
	mutations,
	getters,
});

const useUsers = (store: Store<RootState>) => {
	const users = computed(() => store.getters['users/getUsers']);
	const isLoading = computed(() => store.getters['users/getIsLoading']);
	const isLoaded = computed(() => store.getters['users/getIsLoaded']);
	const hasError = computed(() => store.getters['users/getHasError']);
	const error = computed(() => store.getters['users/getError']);

	if (!isLoaded.value && !isLoading.value && !hasError.value) {
		store.dispatch('users/getUsers');
	}

	return {
		users,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

export { actions, getters, mutations, useUsers };

export default createUsersModule;
