import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import Vue from 'vue';
import soilApiService from '@/services/soilApiService';
import { AsyncState, RootState } from '../types';
import { createAsyncState } from './test-utils';

export interface ApiKeysState {
	create: AsyncState;
	revoke: AsyncState;
}

type ApiKeysStateKey = keyof ApiKeysState;

interface RequestSuccess {
	key: ApiKeysStateKey;
}

interface RequestError {
	key: ApiKeysStateKey;
	error: unknown;
}

function createInitialState(): ApiKeysState {
	return {
		create: createAsyncState(),
		revoke: createAsyncState(),
	};
}

const actions: ActionTree<ApiKeysState, RootState> = {
	async createApiKey({ commit }, label: string): Promise<string> {
		commit('request', 'create');
		try {
			const apiKey = await soilApiService.createApiKey(label);
			commit('requestSuccess', { key: 'create' });
			return apiKey;
		} catch (error) {
			commit('requestError', { key: 'create', error });
		}
		return '';
	},
	async revokeApiKey({ commit }, id: string): Promise<boolean> {
		commit('request', 'revoke');
		try {
			await soilApiService.revokeApiKey(id);
			commit('requestSuccess', { key: 'revoke' });
			return true;
		} catch (error) {
			commit('requestError', { key: 'revoke', error });
		}
		return false;
	},
	acknowledgeError({ commit }, key: ApiKeysStateKey) {
		commit('resetRequest', key);
	},
};

const mutations: MutationTree<ApiKeysState> = {
	request(state, key: ApiKeysStateKey) {
		Vue.set(state[key], 'isLoading', true);
		Vue.set(state[key], 'isLoaded', false);
		Vue.set(state[key], 'error', null);
	},
	requestSuccess(state, { key }: RequestSuccess) {
		Vue.set(state[key], 'isLoading', false);
		Vue.set(state[key], 'isLoaded', true);
		Vue.set(state[key], 'error', null);
	},
	requestError(state, { key, error }: RequestError) {
		Vue.set(state[key], 'isLoading', false);
		Vue.set(state[key], 'isLoaded', false);
		Vue.set(state[key], 'error', error);
	},
	resetRequest(state, key: ApiKeysStateKey) {
		Vue.set(state[key], 'isLoading', false);
		Vue.set(state[key], 'error', null);
	},
};

const getters: GetterTree<ApiKeysState, RootState> = {
	getIsLoading: (state) => (key: ApiKeysStateKey) => state[key].isLoading,
	getIsLoaded: (state) => (key: ApiKeysStateKey) => state[key].isLoaded,
	getHasError: (state) => (key: ApiKeysStateKey) => Boolean(state[key].error),
	getError: (state) => (key: ApiKeysStateKey) => state[key].error,
};

const createApiKeysModule = (): Module<ApiKeysState, RootState> => ({
	namespaced: true,
	state: createInitialState(),
	actions,
	mutations,
	getters,
});

const useCreateApiKey = (store: Store<RootState>) => {
	const createApiKey = (label: string): Promise<string> => store.dispatch('apiKeys/createApiKey', label);
	const isLoading = computed(() => store.getters['apiKeys/getIsLoading']('create'));
	const isLoaded = computed(() => store.getters['apiKeys/getIsLoaded']('create'));
	const hasError = computed(() => store.getters['apiKeys/getHasError']('create'));
	const error = computed(() => store.getters['apiKeys/getError']('create'));
	const clearError = () => store.dispatch('apiKeys/acknowledgeError', 'create');

	return {
		createApiKey,
		isLoading,
		isLoaded,
		hasError,
		error,
		clearError,
	};
};

const useRevokeApiKey = (store: Store<RootState>) => {
	const revokeApiKey = (id: string): Promise<boolean> => store.dispatch('apiKeys/revokeApiKey', id);
	const isLoading = computed(() => store.getters['apiKeys/getIsLoading']('revoke'));
	const isLoaded = computed(() => store.getters['apiKeys/getIsLoaded']('revoke'));
	const hasError = computed(() => store.getters['apiKeys/getHasError']('revoke'));
	const error = computed(() => store.getters['apiKeys/getError']('revoke'));
	const clearError = () => store.dispatch('apiKeys/acknowledgeError', 'revoke');

	return {
		revokeApiKey,
		isLoading,
		isLoaded,
		hasError,
		error,
		clearError,
	};
};

export { actions, getters, mutations, useCreateApiKey, useRevokeApiKey };

export default createApiKeysModule;
