/* eslint-disable @typescript-eslint/no-explicit-any */
import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createApiKeysModule, { actions, ApiKeysState, getters, useCreateApiKey, useRevokeApiKey } from './apiKeys';
import { createAsyncState, createMockApiKeysState } from './test-utils';

jest.mock('../../services/storageService');
jest.mock('../../services/soilApiService');

describe('apiKeys modules', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('createApiKey', () => {
			it('commits `request` mutation with `create` parameter before making request', async () => {
				await (actions.createApiKey as any)({ commit: commitSpy });
				expect(commitSpy).toHaveBeenCalledWith('request', 'create');
			});

			it('commits `requestSuccess` mutation with `create` on success', async () => {
				(soilApiService.createApiKey as jest.Mock).mockResolvedValue('mock api key');

				await (actions.createApiKey as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestSuccess', { key: 'create' });
			});

			it('commits `requestError` mutation with `create` on error', async () => {
				const error = new Error('error');
				(soilApiService.createApiKey as jest.Mock).mockRejectedValueOnce(error);

				await (actions.createApiKey as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestError', { key: 'create', error });
			});

			it('returns created apiKey on success', async () => {
				(soilApiService.createApiKey as jest.Mock).mockResolvedValue('mock api key');

				const actual = await (actions.createApiKey as any)({ commit: commitSpy });

				expect(actual).toEqual('mock api key');
			});

			it('returns empty string on error', async () => {
				const error = new Error('error');
				(soilApiService.createApiKey as jest.Mock).mockRejectedValueOnce(error);

				const actual = await (actions.createApiKey as any)({ commit: commitSpy });

				expect(actual).toEqual('');
			});
		});

		describe('revokeApiKey', () => {
			it('commits `request` mutation with `revoke` parameter before making request', async () => {
				await (actions.revokeApiKey as any)({ commit: commitSpy });
				expect(commitSpy).toHaveBeenCalledWith('request', 'revoke');
			});

			it('commits `requestSuccess` mutation with `revoke` on success', async () => {
				(soilApiService.revokeApiKey as jest.Mock).mockResolvedValue(true);

				await (actions.revokeApiKey as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestSuccess', { key: 'revoke' });
			});

			it('commits `requestError` mutation with `revoke` on error', async () => {
				const error = new Error('error');
				(soilApiService.revokeApiKey as jest.Mock).mockRejectedValueOnce(error);

				await (actions.revokeApiKey as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestError', { key: 'revoke', error });
			});

			it('returns TRUE on success', async () => {
				(soilApiService.revokeApiKey as jest.Mock).mockResolvedValue(true);

				const actual = await (actions.revokeApiKey as any)({ commit: commitSpy });

				expect(actual).toBe(true);
			});

			it('returns FALSE on error', async () => {
				const error = new Error('error');
				(soilApiService.revokeApiKey as jest.Mock).mockRejectedValueOnce(error);

				const actual = await (actions.revokeApiKey as any)({ commit: commitSpy });

				expect(actual).toBe(false);
			});
		});

		describe('acknowledgeError', () => {
			it('commits `resetRequest` mutation with key', async () => {
				await (actions.acknowledgeError as any)({ commit: commitSpy }, 'currentUser');

				expect(commitSpy).toHaveBeenCalledWith('resetRequest', 'currentUser');
			});
		});
	});

	describe('mutations', () => {
		let apiKeysModule: any;

		beforeEach(() => {
			apiKeysModule = createApiKeysModule();
		});

		describe('request', () => {
			it('sets isLoading to true, isLoaded to false, error to null by key', () => {
				apiKeysModule.state = createMockApiKeysState({
					create: createAsyncState({ isLoaded: true, error: 'error' }),
				});

				apiKeysModule.mutations.request(apiKeysModule.state, 'create');

				expect(apiKeysModule.state.create).toHaveProperty('isLoading', true);
				expect(apiKeysModule.state.create).toHaveProperty('isLoaded', false);
				expect(apiKeysModule.state.create).toHaveProperty('error', null);
			});
		});

		describe('requestSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null by key', () => {
				apiKeysModule.state = createMockApiKeysState({
					create: createAsyncState({ isLoading: true, error: 'error' }),
				});

				apiKeysModule.mutations.requestSuccess(apiKeysModule.state, { key: 'create' });

				expect(apiKeysModule.state.create).toHaveProperty('isLoading', false);
				expect(apiKeysModule.state.create).toHaveProperty('isLoaded', true);
				expect(apiKeysModule.state.create).toHaveProperty('error', null);
			});
		});

		describe('requestError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error by key', () => {
				apiKeysModule.state = createMockApiKeysState({
					revoke: createAsyncState({ isLoaded: true, isLoading: true }),
				});

				apiKeysModule.mutations.requestError(apiKeysModule.state, { key: 'revoke', error: 'error' });

				expect(apiKeysModule.state.revoke).toHaveProperty('isLoading', false);
				expect(apiKeysModule.state.revoke).toHaveProperty('isLoaded', false);
				expect(apiKeysModule.state.revoke).toHaveProperty('error', 'error');
			});
		});

		describe('resetRequest', () => {
			it('sets isLoading to false, error to null by key', () => {
				apiKeysModule.state = createMockApiKeysState({
					revoke: createAsyncState({ isLoading: true, error: 'error' }),
				});

				apiKeysModule.mutations.resetRequest(apiKeysModule.state, 'revoke');

				expect(apiKeysModule.state.revoke).toHaveProperty('isLoading', false);
				expect(apiKeysModule.state.revoke).toHaveProperty('error', null);
			});
		});
	});

	describe('getters', () => {
		let state: ApiKeysState;
		beforeEach(() => {
			state = createMockApiKeysState();
		});

		describe('getIsLoading', () => {
			it('returns isLoading getter by key', () => {
				const actual = (getters as any).getIsLoading(state)('create');

				expect(actual).toBe(false);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded getter by key', () => {
				const actual = (getters as any).getIsLoaded(state)('create');

				expect(actual).toBe(false);
			});
		});

		describe('getHasError', () => {
			it('returns getter that returns true if error is not null', () => {
				state.create.error = 'error';

				const actual = (getters as any).getHasError(state)('create');

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const actual = (getters as any).getHasError(state)('create');

				expect(actual).toBe(false);
			});
		});

		describe('getError', () => {
			it('returns error getter by key', () => {
				state.create.error = 'error';

				const actual = (getters as any).getError(state)('create');

				expect(actual).toBe('error');
			});
		});
	});

	describe('hooks', () => {
		describe('useCreateApiKey', () => {
			it('returns computedRefs of api keys state', () => {
				const store = createTestStore();
				const apiKeys = createMockApiKeysState({ create: createAsyncState({ isLoaded: true }) });
				store.replaceState({ ...store.state, apiKeys });

				const actual = useCreateApiKey(store);

				expect(actual.isLoading.value).toBe(false);
				expect(actual.isLoaded.value).toBe(true);
				expect(actual.hasError.value).toBe(false);
				expect(actual.error.value).toBeNull();
			});
		});

		describe('useRevokeApiKey', () => {
			it('returns computedRefs of api keys state', () => {
				const store = createTestStore();
				const apiKeys = createMockApiKeysState({ revoke: createAsyncState({ isLoaded: true }) });
				store.replaceState({ ...store.state, apiKeys });

				const actual = useRevokeApiKey(store);

				expect(actual.isLoading.value).toBe(false);
				expect(actual.isLoaded.value).toBe(true);
				expect(actual.hasError.value).toBe(false);
				expect(actual.error.value).toBeNull();
			});
		});
	});
});
