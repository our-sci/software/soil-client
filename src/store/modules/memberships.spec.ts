import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createMembershipsModule, { actions, useMemberships } from './memberships';

jest.mock('../../services/soilApiService');

describe('memberships module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('getMemberships', () => {
			it('commits requestGetMemberships, then requestGetMembershipsSuccess on getMemberships success', async () => {
				const store = createTestStore();
				jest.spyOn(store, 'commit');

				(soilApiService.getMemberships as jest.Mock).mockResolvedValueOnce({ data: [] });

				await store.dispatch('memberships/getMemberships', 'groupId');

				expect(store.commit).toHaveBeenCalledWith('memberships/requestGetMemberships', 'groupId', undefined);
				expect(store.commit).toHaveBeenCalledWith('memberships/requestGetMembershipsSuccess', [], undefined);
			});

			it('commits requestGetMemberships, then requestGetMembershipsError on getMemberships error', async () => {
				(soilApiService.getMemberships as jest.Mock).mockRejectedValueOnce(new Error('error'));

				await (actions.getMemberships as any)({ commit: commitSpy }, 'groupId');

				expect(commitSpy).toHaveBeenCalledWith('requestGetMemberships', 'groupId');
				expect(commitSpy).toHaveBeenCalledWith('requestGetMembershipsError', new Error('error'));
			});

			it('commits requestGetMemberships, then aborts before committing requestGetMembershipsSuccess if the passed in groupId does not match the currentGroupId', (done) => {
				const store = createTestStore();
				jest.spyOn(store, 'commit');

				(soilApiService.getMemberships as jest.Mock).mockReturnValue(
					new Promise((resolve) => {
						setTimeout(() => resolve({ data: [] }), 0);
					})
				);

				store.dispatch('memberships/getMemberships', 'groupId').then(() => {
					expect(store.commit).toHaveBeenCalledWith('memberships/requestGetMemberships', 'groupId', undefined);
					expect(store.commit).not.toHaveBeenCalledWith('memberships/requestGetMembershipsSuccess', [], undefined);
					done();
				});

				store.state.memberships.currentGroupId = 'groupId2';
			});
		});
	});

	describe('mutations', () => {
		let membershipsModule: any;

		beforeEach(() => {
			membershipsModule = createMembershipsModule();
		});

		describe('requestGetMemberships', () => {
			it('sets isLoading to true, isLoaded to false, and error to null, currentGroupId to groupId', () => {
				membershipsModule.mutations.requestGetMemberships(membershipsModule.state, 'groupId');

				expect(membershipsModule.state.isLoading).toBe(true);
				expect(membershipsModule.state.isLoaded).toBe(false);
				expect(membershipsModule.state.error).toBe(null);
				expect(membershipsModule.state.currentGroupId).toBe('groupId');
			});
		});

		describe('requestGetMembershipsSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null, and memberships to the passed in memberships', () => {
				membershipsModule.mutations.requestGetMembershipsSuccess(membershipsModule.state, []);

				expect(membershipsModule.state.isLoading).toBe(false);
				expect(membershipsModule.state.isLoaded).toBe(true);
				expect(membershipsModule.state.error).toBe(null);
				expect(membershipsModule.state.memberships).toEqual([]);
			});
		});

		describe('requestGetMembershipsError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error, and memberships to an empty array', () => {
				membershipsModule.mutations.requestGetMembershipsError(membershipsModule.state, new Error('error'));

				expect(membershipsModule.state.isLoading).toBe(false);
				expect(membershipsModule.state.isLoaded).toBe(false);
				expect(membershipsModule.state.error).toEqual(new Error('error'));
				expect(membershipsModule.state.memberships).toEqual([]);
			});
		});
	});

	describe('getters', () => {
		let membershipsModule: any;

		beforeEach(() => {
			membershipsModule = createMembershipsModule();
		});

		describe('getIsLoading', () => {
			it('returns isLoading', () => {
				membershipsModule.state.isLoading = true;

				expect(membershipsModule.getters.getIsLoading(membershipsModule.state)).toBe(true);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded', () => {
				membershipsModule.state.isLoaded = true;

				expect(membershipsModule.getters.getIsLoaded(membershipsModule.state)).toBe(true);
			});
		});

		describe('getError', () => {
			it('returns error', () => {
				membershipsModule.state.error = new Error('error');

				expect(membershipsModule.getters.getError(membershipsModule.state)).toEqual(new Error('error'));
			});
		});

		describe('getMemberships', () => {
			it('returns the memberships', () => {
				membershipsModule.state.memberships = [{ id: 'membershipId' }];

				expect(membershipsModule.getters.getMemberships(membershipsModule.state)).toEqual([{ id: 'membershipId' }]);
			});
		});

		describe('getCurrentGroupId', () => {
			it('returns the currentGroupId', () => {
				membershipsModule.state.currentGroupId = 'groupId';

				expect(membershipsModule.getters.getCurrentGroupId(membershipsModule.state)).toBe('groupId');
			});
		});
	});

	describe('hooks', () => {
		let store: any;

		beforeEach(() => {
			store = createTestStore();
		});

		describe('useMemberships', () => {
			it('returns computedRefs of memberships state', () => {
				store.dispatch = jest.fn();
				const actual = useMemberships(store, 'groupId');

				expect(actual.isLoading.value).toBe(false);
				expect(actual.isLoaded.value).toBe(false);
				expect(actual.hasError.value).toBe(false);
				expect(actual.memberships.value).toEqual([]);
			});

			it('dispatches memberships/getMemberships on initial load (no previous memberships have been fetched)', () => {
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).toHaveBeenCalledWith('memberships/getMemberships', 'groupId');
			});

			it('dispatches memberships/getMemberships if groupId has changed', () => {
				store.state.memberships.currentGroupId = 'previousGroupId';
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).toHaveBeenCalledWith('memberships/getMemberships', 'groupId');
			});

			it('dispatches memberships/getMemberships if group has not changed and memberships are not loaded', () => {
				store.state.memberships.currentGroupId = 'groupId';
				store.state.memberships.isLoaded = false;
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).toHaveBeenCalledWith('memberships/getMemberships', 'groupId');
			});

			it('dispatches memberships/getMemberships if group has not changed and memberships are not loading', () => {
				store.state.memberships.currentGroupId = 'groupId';
				store.state.memberships.isLoading = false;
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).toHaveBeenCalledWith('memberships/getMemberships', 'groupId');
			});

			it('dispatches memberships/getMemberships if group has not changed and memberships do not have an error', () => {
				store.state.memberships.currentGroupId = 'groupId';
				store.state.memberships.error = null;
				store.state.memberships.isLoading = false;
				store.state.memberships.isLoaded = false;
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).toHaveBeenCalledWith('memberships/getMemberships', 'groupId');
			});

			it('does NOT dispatch memberships/getMemberships if group changed and memberships have an error', () => {
				store.state.memberships.currentGroupId = 'previousGroupId';
				store.state.memberships.error = new Error('error');
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).not.toHaveBeenCalled();
			});

			it('does NOT dispatch memberships/getMemberships if group is the same and memberships are loading', () => {
				store.state.memberships.currentGroupId = 'groupId';
				store.state.memberships.isLoading = true;
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).not.toHaveBeenCalled();
			});

			it('does NOT dispatch memberships/getMemberships if group is the same and memberships are loaded', () => {
				store.state.memberships.currentGroupId = 'groupId';
				store.state.memberships.isLoaded = true;
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).not.toHaveBeenCalled();
			});

			it('does NOT dispatch memberships/getMemberships if group is the same and memberships have an error', () => {
				store.state.memberships.currentGroupId = 'groupId';
				store.state.memberships.error = new Error('error');
				store.dispatch = jest.fn();

				useMemberships(store, 'groupId');

				expect(store.dispatch).not.toHaveBeenCalled();
			});
		});
	});
});
