import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createUserModule, { actions, useUser } from './user';

jest.mock('../../services/soilApiService');

describe('user module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('getUser', () => {
			it('commits requestGetUser, then requestGetUserSuccess on getUser success', async () => {
				(soilApiService.getUser as jest.Mock).mockResolvedValueOnce({ data: {} });

				await (actions.getUser as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetUser');
				expect(commitSpy).toHaveBeenCalledWith('requestGetUserSuccess', {});
			});

			it('commits requestGetUser, then requestGetUserError on getUser error', async () => {
				(soilApiService.getUser as jest.Mock).mockRejectedValueOnce(new Error('error'));

				await (actions.getUser as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetUser');
				expect(commitSpy).toHaveBeenCalledWith('requestGetUserError', new Error('error'));
			});
		});
	});

	describe('mutations', () => {
		let userModule: any;

		beforeEach(() => {
			userModule = createUserModule();
		});

		describe('requestGetUser', () => {
			it('sets isLoading to true, isLoaded to false, and error to null', () => {
				userModule.mutations.requestGetUser(userModule.state);

				expect(userModule.state.isLoading).toBe(true);
				expect(userModule.state.isLoaded).toBe(false);
				expect(userModule.state.error).toBe(null);
			});
		});

		describe('requestGetUserSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null, and user to the passed in user', () => {
				userModule.mutations.requestGetUserSuccess(userModule.state, {});

				expect(userModule.state.isLoading).toBe(false);
				expect(userModule.state.isLoaded).toBe(true);
				expect(userModule.state.error).toBe(null);
				expect(userModule.state.user).toEqual({});
			});
		});

		describe('requestGetUserError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error, and user to null', () => {
				userModule.mutations.requestGetUserError(userModule.state, new Error('error'));

				expect(userModule.state.isLoading).toBe(false);
				expect(userModule.state.isLoaded).toBe(false);
				expect(userModule.state.error).toEqual(new Error('error'));
			});
		});
	});

	describe('getters', () => {
		let userModule: any;

		beforeEach(() => {
			userModule = createUserModule();
		});

		describe('getIsLoading', () => {
			it('returns isLoading', () => {
				expect(userModule.getters.getIsLoading(userModule.state)).toBe(false);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded', () => {
				expect(userModule.getters.getIsLoaded(userModule.state)).toBe(false);
			});
		});

		describe('getHasError', () => {
			it('returns hasError', () => {
				expect(userModule.getters.getHasError(userModule.state)).toBe(false);
			});
		});

		describe('getError', () => {
			it('returns error', () => {
				expect(userModule.getters.getError(userModule.state)).toBe(null);
			});
		});

		describe('getUser', () => {
			it('returns user', () => {
				expect(userModule.getters.getUser(userModule.state)).toBe(null);
			});
		});
	});

	describe('hooks', () => {
		describe('useUser', () => {
			it('returns computedRefs of user state and dispatches user/getUser if user is not loading or loaded', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();

				const { isLoading, isLoaded, hasError, user } = useUser(store);

				expect(isLoading.value).toBe(false);
				expect(isLoaded.value).toBe(false);
				expect(hasError.value).toBe(false);
				expect(user.value).toBe(null);

				expect(store.dispatch).toHaveBeenCalledWith('user/getUser');
			});

			it('does NOT dispatch user/getUser if user is loading', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				store.state.user.isLoading = true;

				useUser(store);

				expect(store.dispatch).not.toHaveBeenCalledWith('user/getUser');
			});

			it('does NOT dispatch user/getUser if user is loaded', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				store.state.user.isLoaded = true;

				useUser(store);

				expect(store.dispatch).not.toHaveBeenCalledWith('user/getUser');
			});
		});
	});
});
