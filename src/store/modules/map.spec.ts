import createTestStore from '../../../tests/createTestStore';
import createMapModule, { actions } from './map';
import { MapState } from './map';

const createMockMapState = (): MapState => ({ pointToFly: {} });

describe('maps module', () => {
	describe('actions', () => {
		const commitSpy = jest.fn();

		it("commits 'setPointToFly' mutation", async () => {
			(actions.setPointToFly as any)({ commit: commitSpy }, { mapId: 'map1', markerId: 'config1' });
			expect(commitSpy).toHaveBeenCalledWith('setPointToFly', { mapId: 'map1', markerId: 'config1' });
		});

		it("commits 'clearPointToFly' mutation", async () => {
			(actions.clearPointToFly as any)({ commit: commitSpy }, 'map1');
			expect(commitSpy).toHaveBeenCalledWith('clearPointToFly', 'map1');
		});
	});

	describe('mutations', () => {
		let mapModule: any;

		beforeEach(() => {
			mapModule = createMapModule();
			mapModule.state = createMockMapState();
		});

		describe('setPointToFly', () => {
			it('Should set marker to fly into the map', async () => {
				mapModule.mutations.setPointToFly(mapModule.state, { mapId: 'map1', coordinates: [-30.0, 30.0] });

				expect(mapModule.state.pointToFly).toHaveProperty('map1', [-30.0, 30.0]);
			});
		});

		describe('clearPointToFly', () => {
			it('Should clear marker to fly from the map', async () => {
				const pointToFly = { map1: [-30.0, 30.0] };
				mapModule.state = { ...mapModule.state, pointToFly };
				mapModule.mutations.clearPointToFly(mapModule.state, 'map1');

				expect(mapModule.state.pointToFly).not.toHaveProperty('map1');
			});
		});
	});

	describe('getters', () => {
		let state: any;

		beforeEach(() => {
			state = createMockMapState();
		});

		describe('getPointToFlyByMapId', () => {
			it('should returns a marker to fly for a certain map', () => {
				const store = createTestStore();
				store.replaceState({
					...store.state,
					map: {
						...state,
						pointToFly: { map1: [-30.0, 30.0] },
					},
				});

				expect(store.getters['map/getPointToFlyByMapId']('map1')).toEqual([-30.0, 30.0]);
			});

			it("should returns undefined if map doesn't exist", () => {
				const store = createTestStore();
				store.replaceState({ ...store.state, map: state });

				expect(store.getters['map/getPointToFlyByMapId']('unknown-map')).toBeUndefined();
			});
		});
	});
});
