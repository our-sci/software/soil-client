import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import soilApiService from '@/services/soilApiService';
import { AsyncState, Membership, RootState } from '@/store/types';

export interface MembershipsState extends AsyncState {
	currentGroupId: string;
	memberships: Membership[];
}

function createInitialState(): MembershipsState {
	return {
		isLoading: false,
		isLoaded: false,
		error: null,
		memberships: [],
		currentGroupId: '',
	};
}

const actions: ActionTree<MembershipsState, RootState> = {
	async getMemberships({ commit, getters }, groupId: string): Promise<void> {
		commit('requestGetMemberships', groupId);
		try {
			const { data: memberships } = await soilApiService.getMemberships({ groupId });
			// Abort responses for old requests to avoid race conditions if different groups are requested close together in time
			if (getters.getCurrentGroupId !== groupId) {
				return;
			}
			commit('requestGetMembershipsSuccess', memberships);
		} catch (error) {
			commit('requestGetMembershipsError', error);
		}
	},
};

const mutations: MutationTree<MembershipsState> = {
	requestGetMemberships(state, groupId) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
		state.currentGroupId = groupId;
	},
	requestGetMembershipsSuccess(state, memberships) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
		state.memberships = Object.freeze(memberships);
	},
	requestGetMembershipsError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
		state.memberships = [];
	},
};

const getters: GetterTree<MembershipsState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
	getError: (state) => state.error,
	getMemberships: (state) => state.memberships,
	getCurrentGroupId: (state) => state.currentGroupId,
};

const createMembershipsModule = (): Module<MembershipsState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useMemberships = (store: Store<RootState>, groupId: string) => {
	const currentGroupId = computed<string>(() => store.getters['memberships/getCurrentGroupId']);
	const isLoading = computed<boolean>(() => store.getters['memberships/getIsLoading']);
	const isLoaded = computed<boolean>(() => store.getters['memberships/getIsLoaded']);
	const hasError = computed<boolean>(() => store.getters['memberships/getHasError']);
	const memberships = computed<Membership[]>(() => store.getters['memberships/getMemberships']);

	const alreadyFetched = isLoaded.value || isLoading.value || hasError.value;
	const groupHasNotChanged = currentGroupId.value === groupId;
	const groupHasChanged = currentGroupId.value !== groupId;
	const isInitialLoad = currentGroupId.value === '';

	if ((groupHasChanged && !hasError.value) || ((isInitialLoad || groupHasNotChanged) && !alreadyFetched)) {
		store.dispatch('memberships/getMemberships', groupId);
	}

	return {
		isLoading,
		isLoaded,
		hasError,
		memberships,
	};
};

export { actions, getters, mutations, useMemberships };

export default createMembershipsModule;
