import Vuex, { StoreOptions } from 'vuex';
import createAreasModule from '@/store/modules/areas';
import createFieldsModule from '@/store/modules/fields';
import createFieldsViewModule from '@/store/modules/fieldsView';
import createGroupsModule from '@/store/modules/group';
import createMapModule from '@/store/modules/map';
import createMembershipsModule from '@/store/modules/memberships';
import offlineMaps from '@/store/modules/offlineMaps';
import createReassignModule from '@/store/modules/reassign';
import createResourcesModule from '@/store/modules/resources';
import samples from '@/store/modules/samples';
import samplingCollections from '@/store/modules/samplingCollections';
import samplings from '@/store/modules/samplings';
import createStratificationsModule from '@/store/modules/stratifications';
import createUserModule from '@/store/modules/user';
import createUsersModule from '@/store/modules/users';
import caches from '@/store/plugins/caches';
import cleanupDrafts from '@/store/plugins/cleanupDrafts';
import fieldsViewToUrl from '@/store/plugins/fieldsViewToUrl';
import redirects from '@/store/plugins/redirects';
import { RootState } from '@/store/types';
import createApiKeysModule from './modules/apiKeys';
import getters from './getters';

// Vuex mutates the options object used to create the store,
// which is why we have a function to create a new config object
// as opposed to an object literal here.
export function createStoreConfig() {
	const storeConfig: StoreOptions<RootState> = {
		state: {
			version: '0.0.1',
		},
		modules: {
			samplingCollections,
			samplings,
			samples,
			offlineMaps,
			user: createUserModule(),
			users: createUsersModule(),
			fieldsView: createFieldsViewModule(),
			resources: createResourcesModule(),
			groups: createGroupsModule(),
			memberships: createMembershipsModule(),
			reassign: createReassignModule(),
			fields: createFieldsModule(),
			areas: createAreasModule(),
			stratifications: createStratificationsModule(),
			map: createMapModule(),
			apiKeys: createApiKeysModule(),
		},
		plugins: [caches, cleanupDrafts, redirects, fieldsViewToUrl],
		strict: process.env.NODE_ENV !== 'production',
		getters,
	};
	return storeConfig;
}

export default function createStore() {
	const storeConfig = createStoreConfig();
	return new Vuex.Store<RootState>(storeConfig);
}
