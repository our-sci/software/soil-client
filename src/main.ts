import VueCompositionAPI from '@vue/composition-api';
import Vuex from 'vuex';
import Vue from 'vue';
import frag from 'vue-frag';
import createStore from '@/store';
import { injectRouter } from '@/store/plugins/redirects';
import './mapboxMonkeyPatch';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import createRouter from './router';

Vue.config.productionTip = false;

Vue.use(VueCompositionAPI);

Vue.use(Vuex);

Vue.directive('frag', frag);

const store = createStore();

const router = createRouter(store);

injectRouter(router);

new Vue({
	router,
	store,
	vuetify,
	render: (h) => h(App),
}).$mount('#app');

export { router };
