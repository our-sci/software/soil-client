/* eslint-disable @typescript-eslint/no-explicit-any */

const AUTH_TOKEN_KEY = 'auth_token';
const SETTINGS_DISTANCE_UNIT_KEY = 'settings_distance_unit';
const SETTINGS_HIDE_LOCATION_NUMBER_KEY = 'settings_hide_location_number';

const AuthService = {
	setToken(token: string) {
		localStorage.setItem(AUTH_TOKEN_KEY, token);
		window.dispatchEvent(new Event('authservicetokenchange'));
	},
	getToken() {
		return localStorage.getItem(AUTH_TOKEN_KEY) || '';
	},
	clearToken() {
		localStorage.removeItem(AUTH_TOKEN_KEY);
		window.dispatchEvent(new Event('authservicetokenchange'));
	},
};

const SettingsService = {
	getDistanceUnit() {
		return localStorage.getItem(SETTINGS_DISTANCE_UNIT_KEY) || 'm';
	},
	setDistanceUnit(unit: any) {
		localStorage.setItem(SETTINGS_DISTANCE_UNIT_KEY, unit);
	},
	clearDistanceUnit() {
		localStorage.removeItem(SETTINGS_DISTANCE_UNIT_KEY);
	},
	getHideLocationNumber() {
		return localStorage.getItem(SETTINGS_HIDE_LOCATION_NUMBER_KEY) === 'true';
	},
	setHideLocationNumber(unit: any) {
		localStorage.setItem(SETTINGS_HIDE_LOCATION_NUMBER_KEY, String(unit));
	},
	clearHideLocationNumber() {
		localStorage.removeItem(SETTINGS_HIDE_LOCATION_NUMBER_KEY);
	},
};

export { AuthService, SettingsService };
