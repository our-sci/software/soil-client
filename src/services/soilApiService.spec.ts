import axios from 'axios';
import {
	createMockAddress,
	createMockArea,
	createMockContactPoint,
	createMockMultiPolygon,
} from '../../tests/mockGenerators';
import soilApiService from './soilApiService';
import { AuthService } from './storageService';

jest.mock('axios');
jest.mock('./storageService.ts', () => ({
	AuthService: {
		getToken: () => 'mock-token',
		setToken: jest.fn(),
	},
}));

describe('soilApiService', () => {
	describe('getAllResources', () => {
		it("calls the Soil API's GET /resources endpoint with Authorization header", async () => {
			await soilApiService.getAllResources();

			expect(axios.get).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/resources`, {
				headers: { Authorization: 'Bearer mock-token' },
			});
		});

		it("returns resources from the response from the Soil API's GET /resources endpoint", async () => {
			(axios.get as jest.Mock).mockResolvedValueOnce({
				data: {
					fields: [],
					areas: [],
					stratifications: [],
					locationCollections: [],
					locations: [],
					samples: [],
					samplings: [],
					samplingCollections: [],
				},
			});

			const actual = await soilApiService.getAllResources();

			expect(actual).toEqual({
				fields: [],
				areas: [],
				stratifications: [],
				locationCollections: [],
				locations: [],
				samples: [],
				samplings: [],
				samplingCollections: [],
			});
		});
	});

	describe('submitSamplingCollection', () => {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let actual: any;

		beforeEach(async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({
				data: {
					samples: [],
					samplings: [],
					samplingCollections: [],
				},
			});

			actual = await soilApiService.submitSamplingCollection({}, '123');
		});

		it("calls the Soil API's POST /sampling-collections endpoint with Authorization header", () => {
			expect((axios.post as jest.Mock).mock.calls[0][2]).toEqual({
				headers: { Authorization: 'Bearer mock-token' },
			});
		});

		it("calls the Soil API's POST location-collections/:locationCollectionId/sampling-collections endpoint with the locationCollectionId in the path", () => {
			expect((axios.post as jest.Mock).mock.calls[0][0]).toEqual(
				`${process.env.VUE_APP_SOIL_API_URL}/location-collections/123/sampling-collections`
			);
		});

		it("calls the Soil API's POST /sampling-collections endpoint, passing the populatedSamplingCollection as the POST body", () => {
			expect((axios.post as jest.Mock).mock.calls[0][1]).toEqual({});
		});

		it("returns resources from the response from the Soil API's POST /sampling-collections endpoint", () => {
			expect(actual).toEqual({
				samples: [],
				samplings: [],
				samplingCollections: [],
			});
		});
	});

	describe('getGroups', () => {
		it("calls the Soil API's GET /groups endpoint with Authorization header", async () => {
			await soilApiService.getGroups();

			expect(axios.get).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/groups`, {
				headers: { Authorization: 'Bearer mock-token' },
			});
		});
	});

	describe('reassign', () => {
		beforeEach(async () => await soilApiService.reassign({ groupId: 'mock group id', fieldId: 'mock field id' }));

		it("calls the Soil API's PATCH /reassign endpoint with Authorization header", async () => {
			expect(axios.patch).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/reassign`, expect.anything(), {
				headers: { Authorization: 'Bearer mock-token' },
			});
		});

		it("calls the Soil API's PATCH /reassign endpoint with body containing groupId and fieldId", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/reassign`,
				{ groupId: 'mock group id', fieldId: 'mock field id' },
				expect.anything()
			);
		});
	});

	describe('createField', () => {
		beforeEach(
			async () =>
				await soilApiService.createField({
					groupId: 'mock group id',
					name: 'mock field name',
					producerName: 'mock producer name',
					address: createMockAddress(),
					contactPoints: [createMockContactPoint()],
					areas: [
						{
							type: 'Feature',
							properties: {},
							geometry: createMockMultiPolygon(),
						},
					],
				})
		);

		it("calls the Soil API's POST /fields endpoint with group id", async () => {
			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/fields?groupId=mock group id`,
				expect.anything(),
				expect.anything()
			);
		});

		it("calls the Soil API's POST /fields endpoint with Authorization header", async () => {
			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/fields?groupId=mock group id`,
				expect.anything(),
				{
					headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }),
				}
			);
		});

		it("calls the Soil API's POST /fields endpoint with a modified field", async () => {
			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/fields?groupId=mock group id`,
				{
					name: 'mock field name',
					producerName: 'mock producer name',
					address: createMockAddress(),
					contactPoints: [createMockContactPoint()],
					areas: [
						{
							type: 'Feature',
							properties: {},
							geometry: createMockMultiPolygon(),
						},
					],
				},
				expect.anything()
			);
		});
	});

	describe('editField', () => {
		beforeEach(
			async () =>
				await soilApiService.editField({
					id: 'mock field id',
					name: 'mock field name',
					producerName: 'mock producer name',
					address: createMockAddress(),
					contactPoints: [createMockContactPoint()],
				})
		);

		it("calls the Soil API's PATCH /fields/:id endpoint with Authorization header", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/fields/mock field id`,
				expect.anything(),
				{ headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }) }
			);
		});

		it("calls the Soil API's PATCH /fields/:id endpoint with a modified field", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/fields/mock field id`,
				{
					name: 'mock field name',
					producerName: 'mock producer name',
					address: createMockAddress(),
					contactPoints: [createMockContactPoint()],
				},
				expect.anything()
			);
		});
	});

	describe('deleteField', () => {
		it("calls the Soil API's DELETE /fields/:id endpoint with Authorization header", async () => {
			await soilApiService.deleteField('mock field id');

			expect(axios.delete).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/fields/mock field id`, {
				headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }),
			});
		});
	});

	describe('deleteStratification', () => {
		it("calls the Soil API's DELETE /stratifications/:id endpoint", async () => {
			await soilApiService.deleteStratification('mock-stratification-id');

			expect(axios.delete).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/stratifications/mock-stratification-id?force=false`,
				{
					headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }),
				}
			);
		});

		it('sets the force query parameter to the passed value', async () => {
			await soilApiService.deleteStratification('mock-stratification-id', true);

			expect(axios.delete).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/stratifications/mock-stratification-id?force=true`,
				{
					headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }),
				}
			);
		});
	});

	describe('editArea', () => {
		const mockArea = createMockArea({
			geometry: createMockMultiPolygon(),
			properties: {
				name: 'mock area name',
				description: 'mock area description',
				featureOfInterest: 'mock feature of interest',
			},
		});

		beforeEach(async () => await soilApiService.editArea(mockArea));

		it("calls the Soil API's PATCH /areas/:id endpoint with Authorization header", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/areas/mock area id`,
				expect.anything(),
				{ headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }) }
			);
		});

		it("calls the Soil API's PATCH /areas/:id endpoint with a modified area, omitting properties.featureOfInterest", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/areas/mock area id`,
				{
					type: 'Feature',
					geometry: {
						type: 'MultiPolygon',
						coordinates: [
							[
								[
									[-80, 40],
									[-80, 39],
									[-79, 39],
									[-80, 40],
								],
							],
						],
					},
					properties: {
						name: 'mock area name',
						description: 'mock area description',
					},
				},
				expect.anything()
			);
		});
	});

	describe('editSample', () => {
		beforeEach(
			async () =>
				await soilApiService.editSample({
					id: 'mock id',
					name: 'mock sample name',
					soDepth: {
						minValue: { unit: 'CM', value: 0 },
						maxValue: { unit: 'CM', value: 30 },
					},
				})
		);

		it("calls the Soil API's PATCH /samples/:id endpoint with Authorization header", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/samples/mock id`,
				expect.anything(),
				{ headers: expect.objectContaining({ Authorization: 'Bearer mock-token' }) }
			);
		});

		it("calls the Soil API's PATCH /samples/:id endpoint with a modified area", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/samples/mock id`,
				{
					name: 'mock sample name',
					soDepth: {
						minValue: { unit: 'CM', value: 0 },
						maxValue: { unit: 'CM', value: 30 },
					},
				},
				expect.anything()
			);
		});
	});

	describe('getUser', () => {
		it("calls the Soil API's /user endpoint", async () => {
			await soilApiService.getUser();

			expect(axios.get).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/user`, {
				headers: { Authorization: 'Bearer mock-token' },
			});
		});
	});

	describe('getUsers', () => {
		it("calls the Soil API's GET /users endpoint", async () => {
			await soilApiService.getUsers();

			expect(axios.get).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/users`, {
				headers: {
					Authorization: 'Bearer mock-token',
				},
			});
		});
	});

	describe('createApiKey', () => {
		it("calls the Soil API's POST /api-keys endpoint with label and returns apiKey from the response", async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({
				data: { id: 'mock id', label: 'mock label', apiKey: 'mock api key' },
			});

			const actual = await soilApiService.createApiKey('mock label');

			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/api-keys`,
				{ label: 'mock label' },
				{
					headers: { Authorization: 'Bearer mock-token' },
				}
			);
			expect(actual).toEqual('mock api key');
		});
	});

	describe('revokeApiKey', () => {
		it("calls the Soil API's DELETE /api-keys/:id endpoint", async () => {
			await soilApiService.revokeApiKey('mock api key id');

			expect(axios.delete).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/api-keys/mock api key id`, {
				headers: { Authorization: 'Bearer mock-token' },
			});
		});
	});

	describe('register', () => {
		it("calls the Soil API's /auth/register endpoint and saves the token from the response", async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({ data: { token: 'mock-token' } });

			await soilApiService.register({ email: 'mock email', password: 'mock password' });

			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/auth/register`,
				expect.objectContaining({ email: 'mock email', password: 'mock password' })
			);
			expect(AuthService.setToken).toHaveBeenCalledWith('mock-token');
		});
	});

	describe('login', () => {
		it("calls the Soil API's /auth/login endpoint and saves the token from the response", async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({ data: { token: 'mock-token' } });

			await soilApiService.login({ email: 'mock email', password: 'mock password' });

			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/auth/login`,
				expect.objectContaining({ email: 'mock email', password: 'mock password' })
			);
			expect(AuthService.setToken).toHaveBeenCalledWith('mock-token');
		});
	});

	describe('requestPasswordReset', () => {
		it("calls the Soil API's /auth/request-password-reset endpoint", async () => {
			await soilApiService.requestPasswordReset({ email: 'mock email' });

			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/auth/request-password-reset`,
				expect.objectContaining({ email: 'mock email' })
			);
		});
	});

	describe('resetPassword', () => {
		it("calls the Soil API's /auth/reset-password endpoint and saves the token from the response", async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({ data: { token: 'mock-token' } });

			await soilApiService.resetPassword({ password: 'mock password', passwordResetToken: 'mock reset token' });

			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/auth/reset-password`,
				expect.objectContaining({ password: 'mock password', token: 'mock reset token' })
			);
			expect(AuthService.setToken).toHaveBeenCalledWith('mock-token');
		});
	});

	describe('updatePassword', () => {
		it("calls the Soil API's /auth/update-password endpoint and saves the token from the response", async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({ data: { token: 'mock-token' } });

			await soilApiService.updatePassword({ newPassword: 'mock password', currentPassword: 'mock current password' });

			expect(axios.post).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/auth/update-password`,
				expect.objectContaining({ newPassword: 'mock password', currentPassword: 'mock current password' }),
				expect.objectContaining({
					headers: { Authorization: 'Bearer mock-token' },
				})
			);
			expect(AuthService.setToken).toHaveBeenCalledWith('mock-token');
		});
	});
});
