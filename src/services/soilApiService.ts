import axios from 'axios';
import {
	AddMembershipRequest,
	ApiKey,
	Area,
	CreateFieldRequest,
	DeleteGroupRequest,
	DeleteMembershipRequest,
	EditFieldRequest,
	EditSampleRequest,
	GetMembershipRequest,
	ReassignFieldRequest,
	UserWithApiKeys,
} from '@/store/types';
import { AuthService } from './storageService';

const getAllResources = async () => {
	const response = await axios.get(`${process.env.VUE_APP_SOIL_API_URL}/resources`, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});
	return response?.data;
};

const submitSamplingCollection = async (populatedSamplingCollection: object, locationCollectionId: string) => {
	const response = await axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/location-collections/${locationCollectionId}/sampling-collections`,
		populatedSamplingCollection,
		{
			headers: {
				Authorization: `Bearer ${AuthService.getToken()}`,
			},
		}
	);
	return response?.data;
};

const createMembership = ({ groupId, userEmail }: AddMembershipRequest) =>
	axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/groups/${groupId}/memberships`,
		{ userEmail },
		{ headers: { Authorization: `Bearer ${AuthService.getToken()}` } }
	);

const getMemberships = ({ groupId }: GetMembershipRequest) =>
	axios.get(`${process.env.VUE_APP_SOIL_API_URL}/groups/${groupId}/memberships`, {
		headers: { Authorization: `Bearer ${AuthService.getToken()}` },
	});

const deleteMembership = ({ groupId, membershipId }: DeleteMembershipRequest) =>
	axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/groups/${groupId}/memberships/${membershipId}`, {
		headers: { Authorization: `Bearer ${AuthService.getToken()}` },
	});

const createGroup = async ({ parentGroupId, name }: { parentGroupId: string; name: string }) =>
	axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/groups${parentGroupId ? `?parentGroupId=${parentGroupId}` : ''}`,
		{ name },
		{
			headers: { Authorization: `Bearer ${AuthService.getToken()}` },
		}
	);

const editGroup = async ({ groupId, apiOnly }: { groupId: string; apiOnly: boolean }) =>
	axios.patch(
		`${process.env.VUE_APP_SOIL_API_URL}/groups/${groupId}`,
		{ apiOnly },
		{
			headers: { Authorization: `Bearer ${AuthService.getToken()}` },
		}
	);

const getGroups = async () =>
	axios.get(`${process.env.VUE_APP_SOIL_API_URL}/groups`, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const deleteGroup = ({ groupId }: DeleteGroupRequest) =>
	axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/groups/${groupId}`, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const reassign = ({ groupId, fieldId }: ReassignFieldRequest) =>
	axios.patch(
		`${process.env.VUE_APP_SOIL_API_URL}/reassign`,
		{ groupId, fieldId },
		{
			headers: {
				Authorization: `Bearer ${AuthService.getToken()}`,
			},
		}
	);

const createField = ({ groupId, ...field }: CreateFieldRequest) =>
	axios.post(`${process.env.VUE_APP_SOIL_API_URL}/fields?groupId=${groupId}`, field, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const editField = ({ id, ...field }: EditFieldRequest) =>
	axios.patch(`${process.env.VUE_APP_SOIL_API_URL}/fields/${id}`, field, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const deleteField = (fieldId: string) =>
	axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/fields/${fieldId}`, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const deleteStratification = (stratificationId: string, force = false) =>
	axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/stratifications/${stratificationId}?force=${force}`, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const editArea = ({ id, ...area }: Area) => {
	delete area.properties?.featureOfInterest;
	return axios.patch(`${process.env.VUE_APP_SOIL_API_URL}/areas/${id}`, area, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});
};

const editSample = ({ id, ...sample }: EditSampleRequest) =>
	axios.patch(`${process.env.VUE_APP_SOIL_API_URL}/samples/${id}`, sample, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const register = async ({ email, password }: { email: string; password: string }) => {
	const {
		data: { token },
	} = await axios.post(`${process.env.VUE_APP_SOIL_API_URL}/auth/register`, { email, password });
	AuthService.setToken(token);
};

const login = async ({ email, password }: { email: string; password: string }) => {
	const {
		data: { token },
	} = await axios.post(`${process.env.VUE_APP_SOIL_API_URL}/auth/login`, { email, password });
	AuthService.setToken(token);
};

const requestPasswordReset = ({ email }: { email: string }) =>
	axios.post(`${process.env.VUE_APP_SOIL_API_URL}/auth/request-password-reset`, { email });

const resetPassword = async ({ password, passwordResetToken }: { password: string; passwordResetToken: string }) => {
	const {
		data: { token },
	} = await axios.post(`${process.env.VUE_APP_SOIL_API_URL}/auth/reset-password`, {
		password,
		token: passwordResetToken,
	});
	AuthService.setToken(token);
};

const updatePassword = async ({ newPassword, currentPassword }: { newPassword: string; currentPassword: string }) => {
	const {
		data: { token },
	} = await axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/auth/update-password`,
		{ newPassword, currentPassword },
		{ headers: { Authorization: `Bearer ${AuthService.getToken()}` } }
	);
	AuthService.setToken(token);
};

const getUser = () =>
	axios.get<UserWithApiKeys>(`${process.env.VUE_APP_SOIL_API_URL}/user`, {
		headers: { Authorization: `Bearer ${AuthService.getToken()}` },
	});

const getUsers = async () =>
	axios.get<UserWithApiKeys[]>(`${process.env.VUE_APP_SOIL_API_URL}/users`, {
		headers: {
			Authorization: `Bearer ${AuthService.getToken()}`,
		},
	});

const createApiKey = async (label: string) => {
	const response = await axios.post<Required<ApiKey>>(
		`${process.env.VUE_APP_SOIL_API_URL}/api-keys`,
		{ label },
		{ headers: { Authorization: `Bearer ${AuthService.getToken()}` } }
	);
	return response?.data?.apiKey || '';
};

const revokeApiKey = (id: string) =>
	axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/api-keys/${id}`, {
		headers: { Authorization: `Bearer ${AuthService.getToken()}` },
	});

const getStratificationJobs = () =>
	axios.get(`${process.env.VUE_APP_SOIL_API_URL}/stratification-jobs`, {
		headers: { Authorization: `Bearer ${AuthService.getToken()}` },
	});

const stratify = (areaIds: string[], stratificationConfig: any, autoApprove: boolean) =>
	axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/areas/stratify`,
		{ areaIds, stratificationConfig, autoApprove },
		{ headers: { Authorization: `Bearer ${AuthService.getToken()}` } }
	);

const closeStratificationJob = (stratificationJobId: string, closedReason: string) =>
	axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/stratification-jobs/${stratificationJobId}/close`,
		{ closedReason },
		{ headers: { Authorization: `Bearer ${AuthService.getToken()}` } }
	);

const createStratification = (stratificationRequestBody: any, areaId: string) =>
	axios.post(`${process.env.VUE_APP_SOIL_API_URL}/areas/${areaId}/stratifications`, stratificationRequestBody, {
		headers: { Authorization: `Bearer ${AuthService.getToken()}` },
	});

const soilApiService = {
	getAllResources,
	submitSamplingCollection,
	createGroup,
	editGroup,
	getGroups,
	deleteGroup,
	reassign,
	createField,
	editField,
	deleteField,
	deleteStratification,
	editArea,
	editSample,
	getUser,
	getUsers,
	createApiKey,
	revokeApiKey,
	createMembership,
	getMemberships,
	deleteMembership,
	register,
	login,
	requestPasswordReset,
	resetPassword,
	updatePassword,
	getStratificationJobs,
	stratify,
	closeStratificationJob,
	createStratification,
};

export default soilApiService;
