# SoilStack Client

[SoilStack](https://www.soilstack.io) is an open source, cross platform, progressive web app designed to support smart agricultural and environmental sampling, capturing patterns of spatial variability and guiding users through in-field collection in a transparent, replicable, and user-friendly way.

This repository contains the SoilStack frontend. The SoilStack API repository can be found [here](https://gitlab.com/our-sci/software/soil-api).

## Getting Started
  You may need to change some of the variables in `.env` if the defaults do not match your setup.
### Local Development
  - install dependencies: `yarn install`
  - run the dev server: `yarn serve`
  - run tests: `yarn test:unit`
  - run linter: `yarn lint`

### Developing with a Local Instance of SoilStack API
  - Follow the [README for SoilStack API](https://gitlab.com/our-sci/software/soil-api), setting up Postgres and Keycloak.