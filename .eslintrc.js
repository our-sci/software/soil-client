module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: [
		'plugin:vue/essential',
		'eslint:recommended',
		'@vue/typescript/recommended',
		'@vue/prettier/@typescript-eslint',
		'@vue/prettier',
	],
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
	},
	plugins: ['unused-imports', 'simple-import-sort'],
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'@typescript-eslint/no-unused-vars': 'off',
		'unused-imports/no-unused-imports': 'warn',
		'unused-imports/no-unused-vars': [
			'warn',
			{ vars: 'all', varsIgnorePattern: '^_', args: 'after-used', argsIgnorePattern: '^_' },
		],
		'simple-import-sort/imports': [
			'warn',
			{
				groups: [
					[
						// Node.js builtins. You could also generate this regex if you use a `.js` config.
						// For example: `^(${require("module").builtinModules.join("|")})(/|$)`
						'^(assert|buffer|child_process|cluster|console|constants|crypto|dgram|dns|domain|events|fs|http|https|module|net|os|path|punycode|querystring|readline|repl|stream|string_decoder|sys|timers|tls|tty|url|util|vm|zlib|freelist|v8|process|async_hooks|http2|perf_hooks)(/.*|$)',
						// Packages. `vue` related packages come first.
						'^@vue',
						'^vuex',
						'^vuetify',
						'^@?\\w',
						// Internal packages.
						'^(@)(/.*|$)',
						// Side effect imports.
						'^\\u0000',
						// Parent imports. Put `..` last.
						'^\\.\\.(?!/?$)',
						'^\\.\\./?$',
						// Other relative imports. Put same-folder imports and `.` last.
						'^\\./(?=.*/)(?!/?$)',
						'^\\.(?!/?$)',
						'^\\./?$',
						// Style imports.
						'^.+\\.s?css$',
					],
				],
			},
		],
		'simple-import-sort/exports': 'warn',
	},
	overrides: [
		{
			files: ['**/*.spec.{j,t}s?(x)'],
			env: {
				jest: true,
			},
		},
	],
};
